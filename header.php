<?php
  session_start();
?>
<!DOCTYPE html>
<html>
 <head>
     <meta charset="utf-8">
     <meta name="description" content="This is an example of a meta description. This will often show up in search results">
     <meta name=viewport content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <title>Online auction system</title>
 </head>
 <body>

     <header>
         <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
             <a class="navbar-brand" href="#">
                 <img src="img/logo.PNG" width="50" height="50" class="d-inline-block align-top" alt="">WEBSHOP
             </a>
             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                 <ul class="navbar-nav mr-auto">
                     <li class="nav-item active">
                         <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="#">About</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="#">Portfolio</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="#">Bids</a>
                     </li>
                 </ul>
             </div>
             <div>
                 <?php
                 if(isset($_SESSION['userId'])) {
                     echo '<form action="includes/logout.inc.php" method="post">
                     <button type="submit" name="logout-submit" class="btn btn-primary">Logout</button>
                 </form>';
                 }
                 else {
                     echo '
                      <form action="includes/login.inc.php" method="post" >
                     <div class="row">
                         <div class="col">
                             <label class="sr-only" for="inlineFormInputName">e-mail</label>
                             <input type="text" class="form-control" name="mail" placeholder="E-mail...">
                         </div>
                         <div class="col">
                             <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                             <div class="input-group">
                                 <input type="password" class="form-control" name="pwd" placeholder="Password...">
                             </div>
                         </div>
                             <button type="submit" name="login-submit" class="btn btn-primary">Login</button>
                     </div>
                     <a href="signup.php">Not registered? Signup here!</a>
                 </form>
                 ';
                 }
                 ?>
             </div>
         </nav>
         <div class="jumbotron jumbotron-fluid">
             <div class="container">
                 <h1 class="display-4">Welcome to our webshop</h1>
                 <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
             </div>
         </div>
     </header>
