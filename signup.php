<?php
require  "header.php";
?>

    <main>

        <div class="wrapper-main" >
            <section class="section-default">
                <legend><center><h2><b>Registration Form</b></h2></center></legend><br>
                <?php
                  if (isset($_GET['error'])) {
                      if($_GET["error"] == "emptyfields") {
                          echo '<legend><center><h3 class="text-danger"><b>Fill all fields!</b></h3></center></legend><br>';
                      }
                      else if ($_GET["error"] == "invalidmail") {
                          echo '<legend><center><h3 class="text-danger"><b>Invalid e-mail</b></h3></center></legend><br>';
                      }
                      else if ($_GET["error"] == "passwordcheck") {
                          echo '<legend><center><h3 class="text-danger"><b>Your passwords do not match!</b></h3></center></legend><br>';
                      }
                  }
                  else if (isset($_GET["signup"]) == "success") {
                      echo '<legend><center><h3 class="text-success"><b>SUCCESS</b></h3></center></legend><br>';
                  }

                ?>
                <div class="row justify-content-center">
                <form class="well form-horizontal col-lg-6 offset-lg-3" action="includes/signup.inc.php" method="post">
                        <div class="col-md-4 inputGroupContainer">
                        <input type="text" class="form-control" name="name" placeholder="Name">
                    </div>
                        <div class="col-md-4 inputGroupContainer">
                        <input type="text" class="form-control" name="surname" placeholder="Surname">
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 inputGroupContainer">
                        <input type="email" class="form-control" name="mail" aria-describedby="emailHelp" placeholder="E-mail">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                        <div class="col-md-4 inputGroupContainer">
                        <input type="password" class="form-control" name="pwd" placeholder="Password" placeholder="Repeat password">
                    </div>
                        <div class="col-md-4 inputGroupContainer">
                        <input type="password" class="form-control" name="pwd-repeat" placeholder="Repeat password">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2" name="signup-submit">Signup</button>
                </form>
                </div>
            </section>
        </div>
    </main>
<?php
require "footer.php";
?>